# Swedish translation for ucf.
# Copyright (C) 2009, 2014 Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the ucf package.
#
# Martin Bagge <brother@bsnet.se>, 2009, 2014.
# Daniel Nylander <po@danielnylander.se>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ucf 2.002\n"
"Report-Msgid-Bugs-To: ucf@packages.debian.org\n"
"POT-Creation-Date: 2018-02-16 15:56-0800\n"
"PO-Revision-Date: 2014-05-07 11:12+0100\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: Swedish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. Type: title
#. Description
#: ../templates:2001
msgid "Modified configuration file"
msgstr "Ändrad konfigurationsfil"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#. Type: select
#. Choices
#: ../templates:3001 ../templates:4001
msgid "install the package maintainer's version"
msgstr "installera paketansvariges version"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#. Type: select
#. Choices
#: ../templates:3001 ../templates:4001
msgid "keep the local version currently installed"
msgstr "behåll den lokalt installerade version"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#. Type: select
#. Choices
#: ../templates:3001 ../templates:4001
msgid "show the differences between the versions"
msgstr "visa skillnaderna mellan versionerna"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#. Type: select
#. Choices
#: ../templates:3001 ../templates:4001
msgid "show a side-by-side difference between the versions"
msgstr "visa skillnaderna sida vid sida mellan versionerna"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#: ../templates:3001
msgid "show a 3-way difference between available versions"
msgstr "visa en 3-vägs skillnad mellan tillgängliga versioner"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#: ../templates:3001
#, fuzzy
#| msgid "show a 3-way difference between available versions"
msgid "do a 3-way merge between available versions"
msgstr "visa en 3-vägs skillnad mellan tillgängliga versioner"

#. Type: select
#. Choices
#. Translators, please keep translations *short* (less than 65 columns)
#. Type: select
#. Choices
#: ../templates:3001 ../templates:4001
msgid "start a new shell to examine the situation"
msgstr "starta ett nytt skal för att undersöka situationen"

#. Type: select
#. Description
#. Type: select
#. Description
#: ../templates:3002 ../templates:4002
msgid "What do you want to do about modified configuration file ${BASENAME}?"
msgstr "Vad vill du göra med den uppdaterade filen ${BASENAME}?"

#. Type: select
#. Description
#: ../templates:3002
msgid ""
"A new version (${NEW}) of configuration file ${FILE} is available, but the "
"version installed currently has been locally modified."
msgstr ""
"En ny version (${NEW}) av konfigurationsfilen ${FILE} finns tillgänglig, men "
"versionen som är installerad har ändrats lokalt."

#. Type: select
#. Description
#: ../templates:4002
#, fuzzy
#| msgid ""
#| "A new version (${NEW}) of configuration file ${FILE} is available, but "
#| "the version installed currently has been locally modified."
msgid ""
"${BASENAME}: A new version (${NEW}) of configuration file ${FILE} is "
"available, but the version installed currently has been locally modified."
msgstr ""
"En ny version (${NEW}) av konfigurationsfilen ${FILE} finns tillgänglig, men "
"versionen som är installerad har ändrats lokalt."

#. Type: note
#. Description
#: ../templates:5001
msgid "Line by line differences between versions"
msgstr "Visa skillnaderna rad för rad mellan versionerna"

#. Type: error
#. Description
#: ../templates:6001
msgid "Conflicts found in three-way merge"
msgstr "Konflikter i trevägssammanslagning"

#. Type: error
#. Description
#: ../templates:6001
msgid ""
"Conflicts found during three-way merge! Please edit `${dest_file}' and sort "
"them out manually."
msgstr ""
"En eller flera konflikter uppstod vid trevägssammanslagningen! Redigera "
"\"${dest_file}\" och lös konflikterna manuellt."

#. Type: error
#. Description
#: ../templates:6001
msgid ""
"The file `${dest_file}.${ERR_SUFFIX}' has a record of the failed merge of "
"the configuration file."
msgstr ""
"Filen \"${dest_file}.${ERR_SUFFIX}\"  innehåller en notering av den "
"misslyckade sammanslagningen av inställningsfilerna."

#~ msgid "do a 3-way merge between available versions (experimental)"
#~ msgstr "gör en 3-vägs sammanslagning mellan versionerna (experimentell)"

#~ msgid ""
#~ "A new version of configuration file ${FILE} is available, but the version "
#~ "installed currently has been locally modified."
#~ msgstr ""
#~ "En ny version av konfigurationsfilen ${FILE} finns tillgänglig, men "
#~ "versionen som är installerad har ändrats lokalt."
